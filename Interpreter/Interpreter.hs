module Interpreter (Val(..), Expr(..), interpret) where

import Debug.Trace

data Val = IntVal Integer
         | StringVal String
         | BooleanVal Bool
         -- since we are implementing a Functional language, functions are
         -- first class citizens.
         | FunVal [String] Expr Env
     deriving (Show, Eq)

-----------------------------------------------------------

data Expr = Const Val
          -- represents a variable
          | Var String
          -- integer multiplication
          | Expr :*: Expr 
          -- integer addition and string concatenation
          | Expr :+: Expr 
          -- equality test. Defined for all Val except FunVal`
          | Expr :==: Expr 
          -- semantically equivalent to a Haskell `if`
          | If Expr Expr Expr
          -- binds a Var (the first `Expr`) to a value (the second `Expr`), 
          -- and makes that binding available in the third expression
          | Let Expr Expr Expr
          -- creates an anonymous function with an arbitrary number of parameters
          | Lambda [Expr] Expr 
          -- calls a function with an arbitrary number values for parameters
          | Apply Expr [Expr]
     deriving (Show, Eq)

-----------------------------------------------------------

data Env = EmptyEnv 
           | ExtendEnv String Val Env  
           deriving (Show, Eq)

getVarfromEnv:: String -> Env -> Val 
getVarfromEnv searchVar EmptyEnv = error("unbound variable")
getVarfromEnv searchVar (ExtendEnv var val env) = if(searchVar == var) 
														then val 
														else getVarfromEnv searchVar env

----------------------------------------
getParametersName :: [Expr] -> [String]
getParametersName [] = []
getParametersName params = case (head params) of
                              Var str -> str : (getParametersName (tail params))
                              _ -> error("params not defined correctly")

----------------------------------------

extends:: [String] -> [Expr] -> Env -> Env
extends [] [] env = env
extends vars values env = ExtendEnv  (head vars) (evaluate (head values) env) (extends (tail vars) (tail values) env)
-
---------------------------------------
--evaluate funct (extends functPar vals functEnv)
-- the evaluate function takes an environment, which holds variable
-- bindings; i.e. it stores information like `x = 42`
evaluate:: Expr -> Env -> Val
evaluate expr env = 
  trace("expr= " ++ (show expr) ++ "\n env= " ++ (show env)) $
  case expr of
  Const v -> v
  Var str -> getVarfromEnv str env
 
  (e1 :+: e2) -> case ((evaluate e1 env),(evaluate e2 env)) of
  						(IntVal e1, IntVal e2) -> IntVal (e1 + e2)
  						(StringVal e1, StringVal e2) -> StringVal (e1 ++ e2)
  						_ -> error("opertion not permited")
 
  (e1 :*: e2) -> case ((evaluate e1 env),(evaluate e2 env)) of
  						   (IntVal e1, IntVal e2) -> IntVal (e1 * e2)
  						   _ -> error("opertion not permited")
 
  (e1 :==: e2) -> case ((evaluate e1 env),(evaluate e2 env)) of	
  						    (BooleanVal e1, BooleanVal e2) -> BooleanVal (e1 == e2)
  						    (StringVal e1, StringVal e2) -> BooleanVal (e1 == e2)
  						    (IntVal e1, IntVal e2) -> BooleanVal (e1 == e2)
  						    _ -> BooleanVal False
 
  If e1 e2 e3 -> if((evaluate e1 env) == BooleanVal True)
  					        then (evaluate e2 env)
  				 	        else (evaluate e3 env)
 
  Let (Var v) valExpr toEvaluateExpr -> evaluate toEvaluateExpr (extends [v] [valExpr] env)
 
  Lambda params expr -> FunVal (getParametersName params) expr env 
 
  Apply func vals -> case (evaluate func env) of  
                          FunVal params expr env -> evaluate expr (extends params vals env)
 
  _ -> error $ "unimplemented" ++ (show expr)

  -----------------------------------------------------------
-- just an alias that we export in the module definition 
-- (see first line of file)
interpret :: Expr -> Val
interpret expr = evaluate expr EmptyEnv