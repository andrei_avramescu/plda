import Data.Map
import Control.Exception
import Data.Maybe (maybeToList)

-- we define an identifier as tuple. The `Int` will give us an easy way
-- to generate fresh variable names during unification later on.
type Identifier = (Int, String)

----------------------------------------

data Term = Var Identifier
		  | Fun String [Term]
      deriving (Show, Eq)

----------------------------------------
data Clause = Term :- [Term] -- this is a rule
            | Fact Term
     deriving (Show, Eq)

----------------------------------------
-- a Prolog program is nothing but a list of clauses; this is our interpreter's
-- equivalent of the `simple.pl` file.

-- database
type PrologProgram = [Clause]

----------------------------------------
-- we need a substitution, which maps Variables to Terms, denoting that a variable
-- is equal to the term it maps to.

-- current environment
type Substitution = [(Identifier, Term)]
true = []

----------------------------------------
-- your interpreter either returns `No`, meaning that your question could not be answered.
-- or it returns `Yes` with an empty `Substitution` in case your answer is a simple "true",
-- otherwise you get back a substitution mapping Variables to their possible values. 
data Answer = No
            | Yes Substitution
          deriving (Show, Eq)

----------------------------------------
type Depth = Int

----------------------------------------
--lookup :: Substitution -> Identifier -> Term
--lookup subst x =  case Data.Map.lookup subst x of
--				Just x -> x
--				_ -> error "Not found"
 
----------------------------------------
occurence :: Identifier -> Term -> Bool
occurence occurs x = undefined

------------------------------------------
apply_subst :: Substitution -> [Term] -> [Term]
apply_subst subst terms = [apply_subst_term subst t | t <- terms]

------------------------------------------
apply_subst_term :: Substitution -> Term -> [Term]
apply_subst_term subst term = case (subst, term) of
								([], (Var i1)) 							-> [(Var i1)]
								(( (x, strx), t):s , (Var (y, stry))) 	-> if( strx == stry && x == y) then apply_subst_term s t else apply_subst_term s (Var (y, stry))
								(s, (Fun n ts)) 						-> Fun n (apply_subst s ts)

----------------------------------------
getFreshTerm :: Term -> Depth -> Term
getFreshTerm term i = case term of 
						(Var ( _ , str)) -> (Var (i, str))
						(Fun str terms)  -> Fun str (getFreshTerm terms)

getFreshTermList :: [Term] -> Depth -> [Term]
getFreshTermList terms i = [ getFreshTerm term i | term <- terms ]

getFreshAssertions :: PrologProgram -> Depth -> PrologProgram
getFreshAssertions assertions i = [ case a of
						        head :- body -> getFreshTerm head :- getFreshTermList body 
						        Fact body -> Fact getFreshTerm body
						        | a <- assertions]

----------------------------------------
unify :: Term -> Term -> Maybe Substitution
unify t1 t2 = case (t1,t2) of
					(Var i1, Var i2) -> 	[(Var i1, Var i2)]
					(Var i1, i2) 	 -> 	[(Var i1, i2)]
					(i1, Var i2) 	 -> 	[(i1, Var i2)]
					(Fun strx xs , Fun stry ys) -> if (strx == stry) then unify_list xs ys else Nothing
					_							-> Nothing

----------------------------------------
unify_list :: [Term] -> [Term] -> Maybe Substitution
unify_list lst1 lst2 = case (lst1,lst2) of 
							( [] , [] ) -> Just true
							( [] , _ )  -> Nothing
							( _  , [] ) -> Nothing
							( x:xs , y:ys ) -> do 
											s  <- unify x y
											s' <- unify_list (apply_subst s xs) (apply_subst s ys)
											return (s ++ s') 
							_ 				-> Nothing

----------------------------------------
unpack :: Term -> (Term, [Term])
unpack term = case term of 
				head:tail -> (head, tail)
				Fact head -> (head, [])

----------------------------------------
resolveTerm :: PrologProgram -> [Term] -> [(Substitution, [Term])]
resolveTerm assertions (goal:goals) = do 
										let (head, tail) = unpack goal
										assertion <- assertions
										(h , b)   <- unpack assertion
										s 		  <- maybeToList (unify head h)
										return (s, apply_subst s (goals ++ tail))		
							

----------------------------------------
-- obtaining new fresh variables -> resolve goals to subgoals -> resolve subgoals
solve :: PrologProgram -> Depth -> [Term] -> [Substitution]
solve kDB i goals = case goals of 
					[] -> [true]
					_  -> do 
							let newProgram = getFreshAssertions kDB i
							(s, goals') <- resolveTerm newProgram goals
							sol <- solve kDB (i+1) goals'
							return (s ++ sol)

----------------------------------------
prolog :: PrologProgram -> Term -> [Substitution]
prolog knowledgeDB goal = solve knowledgeDB 1 [goal]


