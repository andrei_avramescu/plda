import Data.Map
import Data.Maybe
import Debug.Trace
import Control.Exception
import Data.Typeable
import Data.IORef

data Val = IntVal Integer
         | StringVal String
         | BooleanVal Bool
         -- since we are implementing a Functional language, functions are
         -- first class citizens.
         | FunVal [String] Expr Env
     deriving (Show, Eq)

data Env = EmptyEnv
         | ExtendEnv String Val Env
     deriving (Show, Eq)

------------------------------------------------------------------------------------

data Expr = Const Val
          -- represents a variable
          | Var String
          -- integer multiplication
          | Expr :*: Expr
          -- integer addition and string concatenation
          | Expr :+: Expr
          -- equality test. Defined for all Val except FunVal
          | Expr :==: Expr
          -- semantically equivalent to a Haskell `if`
          | If Expr Expr Expr
          -- binds a a variable to a value (the second `Expr`),
          -- and makes that binding available in the third expression
          | Let Expr Expr Expr
          -- creates an anonymous function with an arbitrary number of parameters
          | Lambda Expr Expr
          -- calls a function with an arbitrary number values for parameters
          | Apply Expr Expr
     deriving (Show, Eq)

------------------------------------------------------------------------------------

data Type = IntT
          | StrT
          | BoolT
          | ConstT Type
          | FunT Type Type
          | TypeVar String
      deriving (Show, Eq)

------------------------------------------------------------------------------------     

type TypeEnvironment = Map String Type

------------------------------------------------------------------------------------
-- this is a type alias, everywhere, within this module, where
-- we could have to use (Map String Type) we can just use Substitution
type Substitution = Map String Type

------------------------------------------------------------------------------------

type Answer = (Type, Substitution)

------------------------------------------------------------------------------------
-- replace each TypeVar in toReplace with replacingType

apply_one_subst :: Type -> String -> Type -> Type
apply_one_subst toReplace typeName replacingType =
          case toReplace of
              IntT                -> trace ("APPLY toReplace : " ++ show(toReplace) ++ " typeName: " ++ show(typeName) ++ " replacingType : " ++ show(replacingType)) IntT
              StrT                -> trace ("APPLY toReplace : " ++ show(toReplace) ++ " typeName: " ++ show(typeName) ++ " replacingType : " ++ show(replacingType)) StrT
              BoolT               -> trace ("APPLY toReplace : " ++ show(toReplace) ++ " typeName: " ++ show(typeName) ++ " replacingType : " ++ show(replacingType)) BoolT
              TypeVar tvar        -> if (tvar == typeName) then trace ("APPLY toReplace : " ++ show(toReplace) ++ " typeName: " ++ show(typeName) ++ " replacingType : " ++ show(replacingType)) replacingType else toReplace
              FunT type1 type2    -> FunT (apply_one_subst type1 typeName replacingType) (apply_one_subst type2 typeName replacingType)

------------------------------------------------------------------------------------
-- replace each type of variable by its binding in the substitution

apply_subst_to_type :: Type -> Substitution -> Type
apply_subst_to_type toReplace subst =
                      case toReplace of
                        IntT              -> IntT
                        BoolT             -> BoolT
                        StrT              -> StrT
                        FunT type1 type2  -> FunT (apply_subst_to_type type1 subst) (apply_subst_to_type type2 subst)
                        TypeVar sn        -> let tmp = Data.Map.lookup sn subst
                                                in case tmp of
                                                    Just x -> x
                                                    _ -> toReplace

------------------------------------------------------------------------------------
-- to be used with subst

--empty_subst :: () -> Substitution
--empty_subst = Data.Map.empty


-- subs t0 for tv0 in all existing binds ++ adds the biding

extended_subst:: Substitution -> String -> Type -> Substitution
extended_subst subst typeName typeValue = Data.Map.map (\x -> (apply_one_subst x typeName typeValue)) subst
                         

------------------------------------------------------------------------------------

no_occurrence :: String -> Type -> Bool
no_occurrence varName typeT =
                              case typeT of
                                IntT  -> True
                                BoolT -> True
                                StrT  -> True
                                FunT typeArg typeResult -> if ((no_occurrence varName typeArg) && (no_occurrence varName typeResult)) then True else False
                                TypeVar name -> if (name == varName) then False else True


------------------------------------------------------------------------------------
-- It takes two types, t1 and t2, a substitution σ that satisfies the no-occurrence invariant, and an expression exp. It returns the substitution that results from adding t1 = t2 to σ

unify:: Type -> Type -> Substitution -> Substitution
unify toReplace replacee substs =
 
   let toReplace2 = apply_subst_to_type toReplace substs
       replacee2 = apply_subst_to_type replacee substs  
  
   in case (toReplace2, replacee2) of

    (IntT , IntT) -> trace ("in typevar : ") substs

    (StrT , StrT) -> trace ("in typevar : ")  substs

    (BoolT , BoolT) -> trace ("in typevar : ")  substs
  
    (ConstT t1, ConstT t2) -> if (t1 == t2) then substs else error "checking error not equal the const"
   
    (TypeVar x, t) -> if (no_occurrence x t) then trace ("UNIFY toReplace : " ++ show(toReplace2) ++ " replacee: " ++ show(replacee) ++ " substs : " ++ show(substs)) (extended_subst substs x t) else error "checking error typevar second variable"
   
    (t, TypeVar y) -> if (no_occurrence y t) then (extended_subst substs y t) else error "checking error typevar first variable"
   
    (FunT typeArg1 typeResult1, FunT typeArg2 typeResult2) ->
                            let subst1 = unify typeArg1 typeArg2 substs
                                subst2 = unify typeResult1 typeResult1 subst1
                            in subst2

    _ -> trace (" usnify ----> toReplace:"  ++ show toReplace ++ " replacee :" ++ show replacee) error "inference checking error unify"
   
------------------------------------------------------------------------------------

infer:: Expr -> TypeEnvironment -> Substitution -> (Type , Substitution)
infer expr tenv subst  =
  case expr of
    Const v -> case v of
                  IntVal _ -> (IntT, subst)
                  StringVal _-> (StrT, subst)
                  BooleanVal _ -> (BoolT, subst)

    Var typeName -> let tmp = Data.Map.lookup typeName subst
                    in case tmp of
                          Just x -> trace ("JustX value: " ++ show x) (x, subst)
                          _ ->  let  fresh = "t" ++ show (Data.Map.size subst)
                                     t = Data.Map.insert typeName (TypeVar fresh) subst
                                in trace ("Adding to subst new:" ++ show fresh ++ " new subst:" ++ show t) ((TypeVar fresh), t) 
   
    expr1 :+: expr2 ->
                      let (e1, subst1) = infer expr1 tenv subst 
                          (e2, subst2) = infer expr2 tenv subst1 
                          subst3 = unify e1 StrT subst2
                          subst4 = unify e2 StrT subst3
                      in trace ("expr1 : " ++ (show expr1) ) trace ("expr2 : " ++ (show expr2)) trace ("+ready " ++ show e1)  trace ("e2 :" ++ show e2) trace("toshow: " ++ show subst3 ++ "substfinal:")  trace (show subst4) (StrT, subst4)
   
    expr1 :==: expr2 ->
                      let (e1, subst1) = infer expr1 tenv subst 
                          (e2, subst2) = infer expr2 tenv subst1 
                          subst3 = unify e1 IntT subst2
                          subst4 = unify e2 IntT subst3
                      in trace ("exp1 : " ++ show e1 ++ " exp2 :" ++ show e2 ++ "subst final : " ++ show subst4) (BoolT, subst4)
   
    expr1 :*: expr2 ->
                      let (e1, subst1) = infer expr1 tenv subst 
                          (e2, subst2) = infer expr2 tenv subst1 
                          subst3 = unify e1 IntT subst2
                          subst4 = unify e2 IntT subst3
                      in trace ("exp1 : " ++ show e1 ++ " exp2 :" ++ show e2 ++ "subst final : " ++ show subst4)  (IntT, subst4)
   
    If expr1 expr2 expr3 -> 
                      let (e1, subst1) = infer expr1 tenv subst 
                          (e2, subst2) = infer expr2 tenv subst1 
                          (e3, subst3) = infer expr3 tenv subst2 
                          subst4 = unify e1 BoolT subst3
                          subst5 = unify e2 e3 subst4
                      in trace ("Exp1: " ++ show e1 ++ " exp2 : " ++ show e2 ++ "exp3 : " ++ show e3 ++ "subst final : " ++ show subst5) (e2, subst5)

    Lambda (Var arg) expr -> let (e1, subst1) = infer (Var arg) tenv subst 
                                 (e2, subst2) = infer expr tenv subst1 
                                 finale1 = fromJust(Data.Map.lookup arg subst2)
                             in trace ("Arg: " ++ show arg ++ " Expr: " ++ show expr ++ " subst final : " ++ show subst2) (FunT finale1 e2, subst2)

    Let (Var arg) val expr -> let (e1,   subst1)  = infer (Var arg) tenv subst
                                  (val2, subst2)  = infer val tenv subst1
                                  subst3          = unify e1 val2 subst2
                                  (ex2,  subst4)  = infer expr tenv subst3
                                  finale_arg = fromJust(Data.Map.lookup arg subst4)
                              in trace ("Arg: " ++ show arg ++ " Expr: " ++ show expr ++ " subst final : " ++ show subst4) (ex2, subst4)

    Apply lambda param ->
                      let (typeOfLambda, subst1)     = infer lambda tenv subst
                          (typeOfParameter, subst2)  = infer param tenv subst
                          --subst1 = unify typeOfLambda typeOfParameter subst
                      in trace ("lambda: " ++ show lambda ++ " param: " ++ show param) (typeOfLambda, subst2)


typeCheckExpr:: Expr -> Type
typeCheckExpr expr = let (typeResult, substFinal ) = infer expr Data.Map.empty Data.Map.empty
                     in typeResult
