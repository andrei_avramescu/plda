import TypeChecker

test1 = 
	assert result StrT "test1"
	where result = typeChecker (If (Const (IntVal 1) :==: Const (IntVal 1)) (Const (StringVal "str1")) (Const (IntVal 42)))



test2 = 
	assert result StrT "test2"
	where result = typeChecker (If (Const (IntVal 1) :==: Const (IntVal 2)) (Const (StringVal "str1")) (Const (StringVal "str2")))



test3 =
	assert result IntT "test3"
	where result = typeChecker (If (Const (IntVal 1) :==: Const (IntVal 1)) (Const (IntVal 42)) (Const (IntVal 42)))



test4 =
	assert result (FunT (TypeVar "t0") StrT) "test4"
	where result = typeChecker (Lambda (Var "x") (If (Const (IntVal 42) :==: Const (IntVal 42)) (Const (StringVal "42")) (Const (StringVal "42"))))


test5 =
	assert result (FunT (TypeVar "t0") (TypeVar "t0")) "test5"
 	where result = typeChecker (Lambda (Var "x") (Var "x"))


test6 =
	assert result (FunT StrT StrT) "test6"
	where result = typeChecker (Lambda (Var "x") ((Var "x") :+: (Const (StringVal "str"))))


test7 =
	assert result IntT "test7"
	where result =  typeChecker (Let (Var "x") (Const (IntVal 42)) (Var "x"))



test8 =
	assert result (FunT IntT StrT) "test8"
	where result = typeChecker (Apply (Lambda (Var "x") ((Var "x") :*: (Const (IntVal 6)))) (Const (IntVal 7)))



testAll = 
  if (allPassed) 
    then "All tests passed."
    else error "Failed tests."
  where allPassed = test1 &&
  					test2 &&
  					test3 &&
  					test4 &&
  					test5 &&
  					test6 &&
  					test7 &&
  					test8



assert :: Type -> Type -> String -> Bool
assert expected received message = 
  if (expected == received) 
    then True
    else error $ message ++ " -> expected: `" ++ (show expected) ++ "`; received: `" ++ (show received) ++ "`"